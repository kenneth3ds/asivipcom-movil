import React from 'react';
import { StyleSheet, Platform, Image, Text, View, TextInput, FlatList, Button } from 'react-native';
import { TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

var contador = 0;
export default class busqueda extends React.Component {
  state = { palabra: '', regionalismos: [], busqueda: '', encontrada: '', prueba: [], contador:0 }

  
  realizarBusqueda = () => {
    var lista_busqueda = [];
    for (var i = 0; i < 3; i++){ //Saca las palabras
      firebase.database().ref('regionalismos/' + i + '/palabra').on('value', snapshot => {
      const data = snapshot.val()
      Object.keys('k').forEach(palabra => lista_busqueda.push(data));
      //Llena la lista con todos los valores de la base para comparar
      if (i == 3) { //keys.length + 1
        this.mostrarBusqueda(lista_busqueda);
      }
      })
    }
  }

  mostrarBusqueda = (lista_busqueda) => {
    var lista_resultado = [];
    for (var i = 0; i < lista_busqueda.length; i++) { //compara hasta recorrer toda la base
      if (lista_busqueda[i] == this.state.busqueda) { //al encontrar la busqueda, la obtiene de la bd
        firebase.database().ref('regionalismos/' + i).on('value', snapshot => {
        const dato = snapshot.val()
        Object.keys(dato).forEach(encontrada => lista_resultado.push(encontrada+': '+ dato[encontrada]));
        Object.keys('\n').forEach(encontrada => lista_resultado.push(''));
        this.setState({ regionalismos : lista_resultado })
        })
      }
    }
  }

  guardarFavoritos = () => {
    if (this.state.regionalismos.length > 0) { //si se ha realizado alguna busqueda
    console.log(this.state.regionalismos);
    firebase.database().ref('favoritos').child(contador).set({
      palabra:this.state.regionalismos[0], //subcampos fijos para cada elemento de la palabra
      pais:this.state.regionalismos[1],
      definicion:this.state.regionalismos[2]
    });
    contador++;
    }
  }


render() {


return (
  <View>
    <View style={{ height: 190, backgroundColor: '#087ca7',
    padding: 15, alignItems: 'center' }}>
      <View style={{ height: 50, width:300, backgroundColor: 'white'  }}>
        <TextInput
        placeholder="Buscar"
        style={styles.titulo}
        onChangeText={ busqueda => this.setState({ busqueda }) }
        value={this.state.busqueda}
        />
      </View>
        <View style = {[{ width: "70%", padding:5 }]}>
          <Button
          title="Buscar"
          onPress={this.realizarBusqueda}
          />
          <Text>{'\n'}</Text>
          <Button
          title="Guardar busqueda en favoritos"
          onPress={this.guardarFavoritos}
        />
        </View>
    </View>
     <FlatList
          data={this.state.regionalismos}
          renderItem={({ item }) => 
          <Text style={{fontSize:20, padding:20, color:'black'}}>{ item }
          </Text>}
          keyExtractor = {(item, index) => index.toString()}
        />
  </View>
    )
  }
}

const styles = StyleSheet.create({
  titulo: {
      fontSize: 20,
      textAlign: 'left',
    },
  container: {
    //flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
