import React from 'react';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation'
import firebase from 'react-native-firebase';

export default class cargando extends React.Component {
  componentDidMount(){
    firebase.auth().onAuthStateChanged(user => {
    this.props.navigation.navigate(user ? 'principal' : 'registro')}
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Cargando</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
