import React from 'react';
import { StyleSheet, Text, TextInput, View, Button, Image } from 'react-native';
import firebase from 'react-native-firebase';

export default class registro extends React.Component {
  state = { email: '', password: '', errorMessage: null }

handleSignUp = () => { //Se invoca esta funcion al presionar boton de crear cuenta
   firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password) 
        .then(() => this.props.navigation.navigate('principal')) //navega a pantalla principal
        .catch(error => this.setState({ errorMessage: error.message }))
  console.log('creacion de cuenta')
}

render() {
    return (
      <View style={styles.container}>
        <Image source={require('./logo_talvez.png')} style={[styles.logo]}/>
         <Text style={styles.titulo}>
          Registro
         </Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
        <TextInput
          placeholder="Correo"
          placeholderTextColor="white"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          placeholder="Contraseña"
          placeholderTextColor="white"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <Text> {'\n'} </Text>
        <Button title="Crear cuenta" onPress={this.handleSignUp} /> //invoca registro
        <Text> {'\n'} </Text>
        <Button title="¿Ya tienes cuenta?"
        onPress={() => this.props.navigation.navigate('login')} 
        /> //navega a inicio de sesion
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#05b2dc'
  },
  titulo: {
      fontSize: 30,
      textAlign: 'center',
      margin: 10,
  },
  logo: {
      height: 180,
      marginBottom: 15,
      marginTop: 10,
      padding: 10,
      width: 200,
  },
  textInput: {
      height: 40,
      width: '60%',
      borderRadius: 20,
      marginTop: 8,
      color: 'white',
      backgroundColor: '#033860'
    }
})
