import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, Image } from 'react-native'
import firebase from 'react-native-firebase'

export default class Login extends React.Component {
  state = { email: '', password: '', errorMessage: null }

  handleLogin = () => {
    const { email, password } = this.state
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password) //verfica usuario y contraseña en firebase
      .then(() => this.props.navigation.navigate('principal')) //navega a pantalla principal
      .catch(error => this.setState({ errorMessage: error.message }))
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./logo_talvez.png')} style={[styles.logo]}/>
        <Text style={styles.titulo}>
           Inicio de sesión
        </Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Correo"
          placeholderTextColor="white"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Contraseña"
          placeholderTextColor="white"
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <Text> {'\n'} </Text>
        <Button title="Iniciar sesión" onPress={this.handleLogin} />
        <Text> {'\n'} </Text>
        <Button title="¿No tienes cuenta?" onPress={() => this.props.navigation.navigate('registro')} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#05b2dc'
  },
  titulo: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  logo: {
    height: 180,
    marginBottom: 15,
    marginTop: 10,
    padding: 10,
    width: 200,
  },
  textInput: {
    height: 40,
    width: '60%',
    borderRadius: 20,
    marginTop: 8,
    color: 'white',
    backgroundColor: '#033860'
  }
})
