import React from 'react';
import { StyleSheet, Platform, Image, Text, View, FlatList } from 'react-native';
import firebase from 'react-native-firebase';


export default class Main extends React.Component {
  state = { palabra: '', regionalismos: [], test: '' }

  //Toma los datos por llave de la base de datos
  //Si se quiere cambiar a todas las llaves -> keys.length
  componentDidMount() { //Esta funcion se ejecuta de forma automatica
    const lista_base = [];
    for (var i = 0; i < 3; i++) {
      firebase.database().ref('regionalismos/' + i).on('value', snapshot => {
      const data = snapshot.val() //referencia la base de datos y guarda el valor
      //Object.keys('k').forEach(palabra => lista_base.push('Palabra, País de origen y definición'));
      Object.keys(data).forEach(palabra => lista_base.push(palabra +': '+ data[palabra]));
      Object.keys('\n').forEach(palabra => lista_base.push(''));
      if (lista_base.length > 0) { //Si existen datos, desplegarlos
        this.setState({ regionalismos : lista_base })
      }
      })
    }
  }


render() {
    const { currentUser } = this.state

return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 80, backgroundColor: '#087ca7' }}>
          <Text style={styles.titulo}>
            Regionalismos {'\n'} Centroamericanos
          </Text>
        </View>
        <FlatList
          data={this.state.regionalismos}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item, index }) => 
          <Text style={{fontSize:22, padding:20, color:'black'}}>{ item }
          </Text>}
          keyExtractor = {(item, index) => index.toString()}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  titulo: {
      fontSize: 23,
      margin: 10,
      textAlign: 'center',
      color: 'white',
      justifyContent: 'center'
    },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
