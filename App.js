import React from 'react';
import firebase from 'react-native-firebase';
import { createAppContainer, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';

import cargando from './src/cargando';
import login from './src/login';
import principal from './src/principal';
import registro from './src/registro';
import busqueda from './src/busqueda';

const App = createAppContainer(createSwitchNavigator(
            {cargando, login, principal, registro, busqueda},
            {initialRouteName: 'cargando'}))

export default App



