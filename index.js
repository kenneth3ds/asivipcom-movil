/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Nav from './Nav';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appName, () => Nav);
