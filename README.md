# Requisitos para correr el proyecto

1. Instalar Node js
2. Instalar Android Studio (opcional)
3. Instalar React

# Instrucciones de ejecución

1. Ingresar por terminal a la ubicación del proyecto
2. Instalar dependencias con el comando: npm install
3. Iniciar emulador de Android Studio o
3.1 Conectar celular Android a la computadora y habilitar "USB Debugging"
4. Ejecutar el comando: npm run android
5. Al mostrarse el mensaje "App running", abrir aplicación en celular o emulador.